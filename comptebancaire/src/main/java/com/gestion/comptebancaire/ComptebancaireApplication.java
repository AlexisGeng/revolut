package com.gestion.comptebancaire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableFeignClients("com.gestion.comptebancaire")
@EnableDiscoveryClient

public class ComptebancaireApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComptebancaireApplication.class, args);
	}
	
	//DOCUMENTER
	//http://localhost:9090/swagger-ui.html#/product-controller
	@Bean
	public Docket swaggerEmplyeeApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.gestion.comptebancaire"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(new ApiInfoBuilder()
						.version("1.0)")
						.title("Intervenant id")
						.description("Documentation intervenant APi v1.0")
						.build());
						
	}
}
