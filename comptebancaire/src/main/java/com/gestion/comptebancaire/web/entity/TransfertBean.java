package com.gestion.comptebancaire.web.entity;

import java.util.Date;



import com.fasterxml.jackson.annotation.JsonFormat;


public class TransfertBean {
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateheure;
	private String iban;
	private Long montant;
	private Long idtransfert;
	private Long idcompte;
	
	public Long getIdcompte() {
		return idcompte;
	}
	public void setIdcompte(Long idcompte) {
		this.idcompte = idcompte;
	}
	

	public Long getIdtransfert() {
		return idtransfert;
	}
	public void setIdtransfert(Long idtransfert) {
		this.idtransfert = idtransfert;
	}
	public Date getDateheure() {
		return dateheure;
	}
	public void setDateheure(Date dateheure) {
		this.dateheure = dateheure;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public Long getMontant() {
		return montant;
	}
	public void setMontant(Long montant) {
		this.montant = montant;
	}
	
	
	
	public TransfertBean(Date dateheure, String iban, Long montant, Long idtransfert, Long idcompte) {
		super();
		this.dateheure = dateheure;
		this.iban = iban;
		this.montant = montant;
		this.idtransfert = idtransfert;
		this.idcompte = idcompte;
	}
	
	public TransfertBean() {
	
	}
	
	
	@Override
	public String toString() {
		return "Transfert [dateheure=" + dateheure + ", iban=" + iban + ", montant=" + montant + ", idtransfert="
				+ idtransfert + ", idcompte=" + idcompte + "]";
	}
	
	
	
	
}
