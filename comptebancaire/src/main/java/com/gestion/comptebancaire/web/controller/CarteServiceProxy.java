package com.gestion.comptebancaire.web.controller;


import java.util.List;


import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.gestion.comptebancaire.web.entity.CarteBancaireBean;



@FeignClient(name="carte-service")
@RibbonClient(name="carte-service")
public interface CarteServiceProxy {
    
  @GetMapping("/comptes/{id}/cartes")
  public List<CarteBancaireBean> getCartes (@PathVariable("id") int id);
  
  @PostMapping("/comptes/{id}/cartes")
  public CarteBancaireBean addCarte (@PathVariable("id") int id, @RequestBody CarteBancaireBean op);
  
  @GetMapping("/comptes/{id}/cartes/{idop}")
  public CarteBancaireBean getCarte(@PathVariable("id") int id, @PathVariable("idop") int idop);
  
  @PutMapping("comptes/{id}/cartes/{idop}")
  public CarteBancaireBean changeCarte(@PathVariable("id") int id,   @PathVariable("idop") int idop, @RequestBody CarteBancaireBean op);
  
  @DeleteMapping("comptes/{id}/cartes/{idop}")
  public CarteBancaireBean deleteCarte(@PathVariable("id") int id,   @PathVariable("idop") int idop);

}