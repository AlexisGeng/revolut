package com.gestion.comptebancaire.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


public class CarteBancaireBean {
	
	private Long id_carte;
	private String numerocarte;
	private String code;
	private String cryptogramme;
	private boolean bloque;
	private boolean localisation;
	private int plafond;
	private boolean sanscontact;
	private boolean virtuelle;
	private int idcompte;
	
	
	
	
	public String getNumerocarte() {
		return numerocarte;
	}







	public void setNumerocarte(String numerocarte) {
		this.numerocarte = numerocarte;
	}







	public boolean isSanscontact() {
		return sanscontact;
	}







	public void setSanscontact(boolean sanscontact) {
		this.sanscontact = sanscontact;
	}







	public int getIdcompte() {
		return idcompte;
	}







	public void setIdcompte(int idcompte) {
		this.idcompte = idcompte;
	}







	public CarteBancaireBean(Long id_carte, String numerocarte, String code, String cryptogramme, boolean bloque,
			boolean localisation, int plafond, boolean sanscontact, boolean virtuelle, int idcompte) {
		super();
		this.id_carte = id_carte;
		this.numerocarte = numerocarte;
		this.code = code;
		this.cryptogramme = cryptogramme;
		this.bloque = bloque;
		this.localisation = localisation;
		this.plafond = plafond;
		this.sanscontact = sanscontact;
		this.virtuelle = virtuelle;
		this.idcompte = idcompte;
	}




	
	
	
	public CarteBancaireBean() {
		super();
	}



	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCryptogramme() {
		return cryptogramme;
	}
	public void setCryptogramme(String cryptogramme) {
		this.cryptogramme = cryptogramme;
	}
	public boolean isBloque() {
		return bloque;
	}
	public void setBloque(boolean bloque) {
		this.bloque = bloque;
	}
	public boolean getLocalisation() {
		return localisation;
	}
	public void setLocalisation(boolean localisation) {
		this.localisation = localisation;
	}
	public int getPlafond() {
		return plafond;
	}
	public void setPlafond(int plafond) {
		this.plafond = plafond;
	}

	public boolean isVirtuelle() {
		return virtuelle;
	}
	public void setVirtuelle(boolean virtuelle) {
		this.virtuelle = virtuelle;
	}


	
	public Long getId_carte() {
		return id_carte;
	}



	public void setId_carte(Long id_carte) {
		this.id_carte = id_carte;
	}













	@Override
	public String toString() {
		return "CarteBancaire [id_carte=" + id_carte + ", numero_carte=" + numerocarte + ", code=" + code
				+ ", cryptogramme=" + cryptogramme + ", bloque=" + bloque + ", localisation=" + localisation
				+ ", plafond=" + plafond + ", sans_contact=" + sanscontact + ", virtuelle=" + virtuelle
				+ ", id_compte=" + idcompte + "]";
	}







	public void setAll(CarteBancaireBean cb) {
		
		
		this.numerocarte = cb.getNumerocarte();
		this.code = cb.getCode();
		this.cryptogramme = cb.getCryptogramme();
		this.bloque = cb.isBloque();
		this.localisation = cb.getLocalisation();
		this.plafond = cb.getPlafond();
		this.sanscontact = cb.isSanscontact();
		this.virtuelle = cb.isVirtuelle();
		this.idcompte = cb.getIdcompte();
		
	}







	
	
}
