package com.gestion.comptebancaire.web.controller;


import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.gestion.comptebancaire.web.entity.DeviseConversionBean;

@FeignClient(name="bourse-service")
@RibbonClient(name="bourse-service")
public interface ConversionProxy {
    
  @GetMapping("/change-devise/source/{source}/cible/{cible}")
  public DeviseConversionBean getValeurChange (@PathVariable("source") String source, 
          @PathVariable("cible") String cible);

}