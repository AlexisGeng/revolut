package com.gestion.comptebancaire.web.controller;

import java.math.BigDecimal;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gestion.comptebancaire.web.entity.DeviseConversionBean;
import com.gestion.comptebancaire.web.entity.OperationBean;

@RestController
public class DeviseConversionRessource {

	@Autowired
    private ConversionProxy proxy;

    @GetMapping("/conversion-devise-feign/source/{source}/cible/{cible}/quantite/{qte}")
    public DeviseConversionBean conversionFeign(@PathVariable String source,
            @PathVariable String cible,
            @PathVariable BigDecimal qte) {

        DeviseConversionBean response = proxy.getValeurChange(source, cible);

        return new DeviseConversionBean(response.getId(), source, cible, response.getTauxConversion(), qte,
                qte.multiply(response.getTauxConversion()), response.getPort());
    }

}
