package com.gestion.comptebancaire.web.controller;


import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestion.comptebancaire.web.entity.DeviseConversionBean;
import com.gestion.comptebancaire.web.entity.OperationBean;
import com.gestion.comptebancaire.web.entity.TransfertBean;

@FeignClient(name="operation-service")
@RibbonClient(name="operation-service")
public interface OperationServiceProxy {
    
  /*@GetMapping("/comptes/{id}/operations")
  public List<OperationBean> getValeurChange (@PathVariable("id") int id);*/
  
  @PostMapping("/comptes/{id}/operations")
  public OperationBean addOperation (@PathVariable("id") int id, @RequestBody OperationBean op);
  
  @GetMapping("/comptes/{id}/operations/{idop}")
  public OperationBean getOperation(@PathVariable("id") int id, @PathVariable("idop") int idop);
  
  
  @GetMapping("/comptes/{id}/transferts")
  public List<TransfertBean> getAllTransfert (@PathVariable("id") int id);
  
  @PostMapping("/comptes/{id}/transferts")
  public TransfertBean addTransfert (@PathVariable("id") int id, @RequestBody TransfertBean op);
  
  @GetMapping("/comptes/{id}/transferts/{idop}")
  public TransfertBean getTransfert(@PathVariable("id") int id, @PathVariable("idop") int idop);

  @GetMapping("/comptes/{id}/operations")
  public  List<OperationBean>  getOperation(@PathVariable("id") int id, @RequestParam Map<String, String> params);

}