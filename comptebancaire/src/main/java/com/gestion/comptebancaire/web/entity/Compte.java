package com.gestion.comptebancaire.web.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="Compte")
public class Compte {
	

	private Long id_compte;
	private String nom;
	private String prenom;
	private Date date_naissance;
	private String pays;
	
	
	
	private int numero_identite;
	private String numero_tel;
	private String iban;
	private String secret;
	private float solde;

	
	
	
	public Compte(Long id_compte, String nom, String prenom, Date date_naissance, String pays, int numero_identite,
			String numero_tel, String iban, String secret, float solde) {
		super();
		this.id_compte = id_compte;
		this.nom = nom;
		this.prenom = prenom;
		this.date_naissance = date_naissance;
		this.pays = pays;
		this.numero_identite = numero_identite;
		this.numero_tel = numero_tel;
		this.iban = iban;
		this.secret = secret;
		this.solde = solde;

	}


	


	public Compte() {
		super();
	}

	
	@Id
	@Column(name="id_compte")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	public Long getid_compte() {
		return id_compte;
	}

	public void setid_compte(Long id_compte) {
		this.id_compte = id_compte;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getdate_naissance() {
		return date_naissance;
	}

	public void setdate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}
	
	
	public int getnumero_identite() {
		return numero_identite;
	}

	public void setnumero_identite(int numero_identite) {
		this.numero_identite = numero_identite;
	}

	public String getnumero_tel() {
		return numero_tel;
	}

	public void setnumero_tel(String numero_tel) {
		this.numero_tel = numero_tel;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}


	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}





	

	public void setAll(Compte cb) {
		
		if(cb.getnumero_tel()!=null) {
			this.numero_tel = cb.getnumero_tel();
		}
		if(cb.getSecret()!=null) {
			this.secret = cb.getSecret();
		}
		
	
	}





	@Override
	public String toString() {
		return "Compte [id_compte=" + id_compte + ", nom=" + nom + ", prenom=" + prenom + ", date_naissance="
				+ date_naissance + ", pays=" + pays + ", numero_identite=" + numero_identite + ", numero_tel="
				+ numero_tel + ", iban=" + iban + ", secret=" + secret + ", solde=" + solde 
				+ "]";
	}
	
	
}
