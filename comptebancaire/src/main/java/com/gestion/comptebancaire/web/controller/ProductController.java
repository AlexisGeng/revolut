package com.gestion.comptebancaire.web.controller;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.tomcat.util.codec.binary.Base64;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gestion.comptebancaire.web.entity.CarteBancaireBean;
import com.gestion.comptebancaire.web.entity.Compte;
import com.gestion.comptebancaire.web.entity.DeviseConversionBean;
import com.gestion.comptebancaire.web.entity.OperationBean;
import com.gestion.comptebancaire.web.entity.TransfertBean;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;





@RestController
@RequestMapping(value="/comptes", produces = MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(Compte.class)
public class ProductController {
		
	    @Autowired
	    private OperationServiceProxy proxy;
	    
	    @Autowired
	    private ConversionProxy proxyConversion;
	    
	    @Autowired
	    private CarteServiceProxy proxyCarte;
	   
	    
	    @PostMapping()
	    public ResponseEntity<?> postCompte(@RequestBody Compte cp) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			
	    	try {
				
	    			
		       		Session session = HibernateUtil.getSessionFactory().openSession();
		       		session.beginTransaction();
		       		
		       			String [] res = this.verifNom();
		       			cp.setNom(res[0]);
		       			cp.setPrenom(res[1]);
						session.save(cp);
						session.getTransaction().commit();
						session.close();
					   
			    		return new ResponseEntity<> (null, HttpStatus.CREATED);
	 
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			
	    }
	    
	    
	  /*  @GetMapping("/{id}/operationsAll")
	    public ResponseEntity<?> conversionFeign(@PathVariable("id") Long id ) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
	    	try {
				if(verifID(id)) {
					List<OperationBean> response = proxy.getValeurChange(Math.toIntExact(id));
					return new ResponseEntity <> (operationToResource(response, id), HttpStatus.OK);

				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
	    	
	    }*/
	    
	    @GetMapping("/{id}/operations")
	    public ResponseEntity<?> getOpeParam(@PathVariable("id") Long id, @RequestParam Map<String, String> params ) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
	    	try {
				if(verifID(id)) {
					List<OperationBean> response = proxy.getOperation(Math.toIntExact(id), params);
					if(response !=null) {
						if(!response.isEmpty()){
							return new ResponseEntity <> (operationToResource(response,id), HttpStatus.OK);
							
						}
						else {
							return new ResponseEntity <> (HttpStatus.BAD_REQUEST);
						}
						
					}
					else {
						return new ResponseEntity <> (HttpStatus.BAD_REQUEST);
					}

				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
	    	
	    }
	    
	    @GetMapping("/{id}/transferts")
	    public ResponseEntity<?> getAllTransfert(@PathVariable("id") Long id ) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
	    	try {
				if(verifID(id)) {
					List<TransfertBean> response = proxy.getAllTransfert((Math.toIntExact(id)));
					return new ResponseEntity <> (transfertToResource(response, id), HttpStatus.OK);
					//return  new ResponseEntity<>(response,HttpStatus.OK);
					/*for (int i = 0; i < response.size(); i++) {
						new OperationBean(response.get(i).getId(),response.get(i).getD(),response.get(i).getLibelle(),response.get(i).getMontant(),response.get(i).getTauxConversion(),response.get(i).getCommercant(),response.get(i).getCategorie(),response.get(i).getPays());
					}*/
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
	    	
	    }
	    
	    @GetMapping("/{id}/transferts/{idop}")
	    public ResponseEntity<?> getTransfert(@PathVariable("id") Long id , @PathVariable("idop") Long idop ) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			try {
				if(verifID(id)) {
					Session session = HibernateUtil.getSessionFactory().openSession();
					session.beginTransaction();
					TransfertBean response = proxy.getTransfert(Math.toIntExact(id),Math.toIntExact(idop));
					//return new ResponseEntity <> (operationToResource(response, id), HttpStatus.OK);
					
					  
					
						try {
							session.getTransaction().commit();
							session.close();
							return new ResponseEntity<>(transfertToResource(response,id,true),HttpStatus.OK);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							session.getTransaction().commit();
							session.close();
							return new ResponseEntity<>(HttpStatus.NOT_FOUND);
						}
					
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
	    	
	    }
	
	    
	    @GetMapping("/{id}/operations/{idop}")
	    public ResponseEntity<?> getOp(@PathVariable("id") Long id, @PathVariable("idop") Long idop ) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			try {
				if(verifID(id)) {
					Session session = HibernateUtil.getSessionFactory().openSession();
					session.beginTransaction();
					OperationBean response = proxy.getOperation(Math.toIntExact(id),Math.toIntExact(idop));
					//return new ResponseEntity <> (operationToResource(response, id), HttpStatus.OK);
					
					  
					
						try {
							session.getTransaction().commit();
							session.close();
							return new ResponseEntity<>(operationToResource(response,id,true),HttpStatus.OK);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							session.getTransaction().commit();
							session.close();
							return new ResponseEntity<>(HttpStatus.NOT_FOUND);
						}
					
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		        	  
	    }
	    
	    @GetMapping("/{id}/cartes/{idop}")
	    public ResponseEntity<?> getCartes(@PathVariable("id") Long id, @PathVariable("idop") Long idop ) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			try {
				if(verifID(id)) {
					Session session = HibernateUtil.getSessionFactory().openSession();
					session.beginTransaction();
					CarteBancaireBean response = proxyCarte.getCarte(Math.toIntExact(id),Math.toIntExact(idop));
					//return new ResponseEntity <> (operationToResource(response, id), HttpStatus.OK);
					
					  
					
						try {
							session.getTransaction().commit();
							session.close();
							return new ResponseEntity<>(carteToResource(response,id,true),HttpStatus.OK);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							session.getTransaction().commit();
							session.close();
							return new ResponseEntity<>(HttpStatus.NOT_FOUND);
						}
					
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		        	  
	    }
	    
	    
	    @DeleteMapping("/{id}/cartes/{idop}")
	    public ResponseEntity<?> deleteCartes(@PathVariable("id") Long id, @PathVariable("idop") Long idop ) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			try {
				if(verifID(id)) {
					Session session = HibernateUtil.getSessionFactory().openSession();
					session.beginTransaction();
					
					//return new ResponseEntity <> (operationToResource(response, id), HttpStatus.OK);
					
					  
					
						try {
							 proxyCarte.deleteCarte(Math.toIntExact(id),Math.toIntExact(idop));
							session.getTransaction().commit();
							session.close();
							return new ResponseEntity<>(HttpStatus.NO_CONTENT);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							session.getTransaction().commit();
							session.close();
							return new ResponseEntity<>(HttpStatus.NOT_FOUND);
						}
					
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		        	  
	    }
	    
	    public String verifPays(String pays) {
	    	System.out.println(pays);
	    	String eur[] = {"France","Allemagne","Italie","Espagne","Belgique","Luxembourg"};
	    	String usd[] = {"USA"};
	    	for (int i = 0; i < eur.length; i++) {
				if(pays.equals(eur[i])) {
					return "EUR";
				}
			}
	    	for (int i = 0; i < usd.length; i++) {
	    		if(pays.equals(usd[i])) {
					return "USD";
				}
			}
	    	return "GBP";
	    }
	    
	  
	    
	    @PostMapping("/{id}/operations")
	    public ResponseEntity<?> addOperations(@PathVariable("id") Long id, @RequestBody OperationBean op ) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			
	    	try {
				if(verifID(id)) {

		       		Session session = HibernateUtil.getSessionFactory().openSession();
		       		session.beginTransaction();
		       		
					Compte cp =  session.get(Compte.class, id);
					if(!op.getPays().equals(cp.getPays())) {
						DeviseConversionBean response = proxyConversion.getValeurChange(verifPays(cp.getPays()), verifPays(op.getPays()));
						op.setTauxconversion(response.getTauxConversion());
						
					
						op.setMontant(op.getMontant().multiply(response.getTauxConversion()));
						OperationBean ope = proxy.addOperation(Math.toIntExact(id), op);
						if(!modifSoldetf(cp, Integer.valueOf(ope.getMontant().intValue()))) {
						
							return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
						}
						session.evict(cp);
						session.update(cp);
        		        session.getTransaction().commit();
        		        session.close();
					    HttpHeaders responseHeader = new HttpHeaders();
			    		responseHeader.setLocation(linkTo(OperationBean.class).slash(ope.getId()).toUri());
			    		return new ResponseEntity<> (null,responseHeader, HttpStatus.CREATED);
					}
					else {
						op.setTauxconversion(null);
						OperationBean ope = proxy.addOperation(Math.toIntExact(id), op);
						session.evict(cp);
						System.out.println(cp);
						if(!modifSoldetf(cp, Integer.valueOf(ope.getMontant().intValue()))) {
							
						
							return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
						}
						session.update(cp);
						System.out.println(cp);
        		        session.getTransaction().commit();
        		        session.close();
					    HttpHeaders responseHeader = new HttpHeaders();
			    		responseHeader.setLocation(linkTo(OperationBean.class).slash(ope.getId()).toUri());
			    		return new ResponseEntity<> (null,responseHeader, HttpStatus.CREATED);
					}
					
						
					
					
			
					/*for (int i = 0; i < response.size(); i++) {
						new OperationBean(response.get(i).getId(),response.get(i).getD(),response.get(i).getLibelle(),response.get(i).getMontant(),response.get(i).getTauxConversion(),response.get(i).getCommercant(),response.get(i).getCategorie(),response.get(i).getPays());
					}*/
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
	    	
	    }
	    
	    @PostMapping("/{id}/cartes")
	    public ResponseEntity<?> addCartes(@PathVariable("id") Long id, @RequestBody CarteBancaireBean op ) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			
	    	try {
				if(verifID(id)) {

		       		Session session = HibernateUtil.getSessionFactory().openSession();
		       		session.beginTransaction();
		       		
					Compte cp =  session.get(Compte.class, id);
					
						
						CarteBancaireBean ope = proxyCarte.addCarte(Math.toIntExact(id), op);
						
					    HttpHeaders responseHeader = new HttpHeaders();
			    		responseHeader.setLocation(linkTo(CarteBancaireBean.class).slash(ope.getId_carte()).toUri());
			    		return new ResponseEntity<> (null,responseHeader, HttpStatus.CREATED);
				
					
						
					
					
			
					/*for (int i = 0; i < response.size(); i++) {
						new OperationBean(response.get(i).getId(),response.get(i).getD(),response.get(i).getLibelle(),response.get(i).getMontant(),response.get(i).getTauxConversion(),response.get(i).getCommercant(),response.get(i).getCategorie(),response.get(i).getPays());
					}*/
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
	    	
	    }
	    
	    public boolean modifSoldetf(Compte cp, float value) {
	    	
	    	float val = cp.getSolde();
	    	System.out.println(val);
	    	System.out.println(value);
	    	if(val <= 0 || val < value) {
	    		System.out.println("not nice");
	    		return false;
	    	}
	    	else {
	    		System.out.println("nice");
	    		val =  val - value ;
		    	cp.setSolde(val);
		    	return true;
	    	}
	    	
	    	
	    }
	    
	    public Compte ibantoid(String iban) throws Exception {
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
	        List<Compte> result = session.createQuery("from Compte").getResultList();
	        for (int i = 0; i < result.size(); i++) {
				if(result.get(i).getIban().equals(iban)) {
					 session.getTransaction().commit();
     		        session.close();
					return result.get(i);
				}
			}
			return null;
	    }
	    
	    @PostMapping("/{id}/transferts")
	    public ResponseEntity<?> addTransfert(@PathVariable("id") Long id, @RequestBody TransfertBean tf ) throws Exception {
	    	System.out.println(tf.toString());
	    	HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			
	    	try {
				if(verifID(id)) {

		       		Session session = HibernateUtil.getSessionFactory().openSession();
		       		session.beginTransaction();
		       		
		       			Compte cp =  session.get(Compte.class, id);
		       			
						TransfertBean tfb = proxy.addTransfert(Math.toIntExact(id), tf);
						Compte cp2 = ibantoid(tfb.getIban());
						
						
							if(cp2 != null) {
								session.evict(cp2);
								cp2.setSolde(cp2.getSolde()+tfb.getMontant());
								session.update(cp2);
							}
						
							session.evict(cp);
						
						if(!modifSoldetf(cp,tfb.getMontant())) {
							return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
						}
						
						session.update(cp);
						
        		        session.getTransaction().commit();
        		        session.close();
					    HttpHeaders responseHeader = new HttpHeaders();
			    		responseHeader.setLocation(linkTo(OperationBean.class).slash(tfb.getIdtransfert()).toUri());
			    		return new ResponseEntity<> (null,responseHeader, HttpStatus.CREATED);
					
					
						
					
					
			
					/*for (int i = 0; i < response.size(); i++) {
						new OperationBean(response.get(i).getId(),response.get(i).getD(),response.get(i).getLibelle(),response.get(i).getMontant(),response.get(i).getTauxConversion(),response.get(i).getCommercant(),response.get(i).getCategorie(),response.get(i).getPays());
					}*/
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
	    	
	    }
		
		@GetMapping(value="/{id}")
		public ResponseEntity<?> getIntervenant(@PathVariable("id") Long id) throws Exception{
			HibernateUtil hb = new HibernateUtil();
			hb.setUp();
			if(verifID(id)) {
				Session session = HibernateUtil.getSessionFactory().openSession();
				session.beginTransaction();
				
				//Optional<Compte> cp = session.byId(Compte.class).loadOptional(id);
				
				
				
		        
		        
				return (ResponseEntity<?>) Optional.ofNullable(session.byId(Compte.class).loadOptional(id))
						.filter(Optional::isPresent).map(i -> {
							try {
								session.getTransaction().commit();
								session.close();
								return new ResponseEntity<>(intervenantToResource(i.get(),true),HttpStatus.OK);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								session.getTransaction().commit();
								session.close();
								return new ResponseEntity<>(HttpStatus.NOT_FOUND);
							}
							
						})
						.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
			}
			else {
				return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			}
			
			
			
			
	    }
		
		 @GetMapping(value = "/secure")
		    public String entryPoint() {
		        return "Securise";
		    }
		    
		    @GetMapping(value = "/hello")
		    public String freeEntryPoint() {
		        return "Open bar";
		    }
		    
		    
			 //GET ALL
			@GetMapping
			public ResponseEntity<?> getAllIntervenants() throws Exception{
				 Authentication authentication = SecurityContextHolder.getContext().getAuthentication(); 
	        	 if(authentication != null) {
	        		 Object details = authentication.getDetails();     
	                 if ( details instanceof OAuth2AuthenticationDetails ){
	                     OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;

	                     Map<String, Object> decodedDetails = (Map<String, Object>)oAuth2AuthenticationDetails.getDecodedDetails();

	                     System.out.println( "My custom claim value: " + oAuth2AuthenticationDetails.getTokenValue() );
	                     String jwtToken =oAuth2AuthenticationDetails.getTokenValue();                     
	                     System.out.println("------------ Decode JWT ------------");
	                     String[] split_string = jwtToken.split("\\.");
	                     String base64EncodedHeader = split_string[0];
	                     String base64EncodedBody = split_string[1];
	                     String base64EncodedSignature = split_string[2];

	                     System.out.println("~~~~~~~~~ JWT Header ~~~~~~~");
	                     Base64 base64Url = new Base64(true);
	                     String header = new String(base64Url.decode(base64EncodedHeader));
	                     System.out.println("JWT Header : " + header);


	                     System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
	                     String body = new String(base64Url.decode(base64EncodedBody));
	                     System.out.println("JWT Body : "+body);  
	                     
	                     JsonParser parser = new JsonParser();

	                   

	                     JsonElement jsonTree = parser.parse(body);

	                     if(jsonTree.isJsonObject()){
	                    	 HibernateUtil hb = new HibernateUtil();
	            				hb.setUp();
	            				Session session = HibernateUtil.getSessionFactory().openSession();
	            				session.beginTransaction();
	                         JsonObject jsonObject = jsonTree.getAsJsonObject();
	                         JsonElement f1 =jsonObject.get("authorities");
	                         String role = f1.toString();
	                         role = role.replace("[", "");
	                         role = role.replace("]", "");
	                         role = role.replace("\"", "");
	                         System.out.println(role);
	                         System.out.println(role.equals("ROLE_ADMIN"));
	                         if(role.equals("ROLE_ADMIN")) {
	                        	 System.out.println("ok");
	                        	 List<Compte> result = session.createQuery("from Compte ").getResultList();
	                        	 session.getTransaction().commit();
	             		        session.close();
	             		       return new ResponseEntity <> (intervenantToResource(result), HttpStatus.OK);
	                         }
	                         JsonElement f2 = jsonObject.get("user_name");
	                         System.out.println("name"+f2.toString());
	                         String name = f2.toString();
               				name = name.replace("\"", "");
               				
            		        List<Compte> result = session.createQuery("from Compte Where nom="+"'"+name+"'").getResultList();
            		        session.getTransaction().commit();
            		        session.close();
            		        return new ResponseEntity <> (intervenantToResource(result), HttpStatus.OK);
	                     }
	                 }
	                 return new ResponseEntity <> (null, HttpStatus.BAD_REQUEST);
	        	 }
	        	 return new ResponseEntity <> (null, HttpStatus.FORBIDDEN);
				
			}
		    
		    //verif id 
		    public boolean verifID(Long idCompte) throws Exception {
		    	Boolean res = false;
	        	 Authentication authentication = SecurityContextHolder.getContext().getAuthentication(); 
	        	 if(authentication != null) {
	        		 Object details = authentication.getDetails();     
	                 if ( details instanceof OAuth2AuthenticationDetails ){
	                     OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;

	                     Map<String, Object> decodedDetails = (Map<String, Object>)oAuth2AuthenticationDetails.getDecodedDetails();

	                     System.out.println( "My custom claim value: " + oAuth2AuthenticationDetails.getTokenValue() );
	                     String jwtToken =oAuth2AuthenticationDetails.getTokenValue();                     
	                     System.out.println("------------ Decode JWT ------------");
	                     String[] split_string = jwtToken.split("\\.");
	                     String base64EncodedHeader = split_string[0];
	                     String base64EncodedBody = split_string[1];
	                     String base64EncodedSignature = split_string[2];

	                     System.out.println("~~~~~~~~~ JWT Header ~~~~~~~");
	                     Base64 base64Url = new Base64(true);
	                     String header = new String(base64Url.decode(base64EncodedHeader));
	                     System.out.println("JWT Header : " + header);


	                     System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
	                     String body = new String(base64Url.decode(base64EncodedBody));
	                     System.out.println("JWT Body : "+body);  
	                     
	                     JsonParser parser = new JsonParser();

	                   

	                     JsonElement jsonTree = parser.parse(body);

	                     if(jsonTree.isJsonObject()){
	                         JsonObject jsonObject = jsonTree.getAsJsonObject();

	                     
	             			Session session = HibernateUtil.getSessionFactory().openSession();
	             			session.beginTransaction();

	                         JsonElement f2 = jsonObject.get("user_name");
	                         System.out.println("name"+f2.toString());
	                         String name = f2.toString();
               				name = name.replace("\"", "");
	                       if(name.equals("admin")) {
	                    	   System.out.println("ok");
	                    	   res = true;
	                       }
	                       else {
	                    	   Compte cp =  (Compte) session.get(Compte.class, idCompte);
		                        
	                  		
	                  			if(cp != null) {
	                  			  if(cp.getNom().equals(name)) {
	 	                        	 res = true;
	 	                         }
	                  			}
	                       }
	                        
	                        
                  		
	                       
	                         
	                     /*
	                     		
	                             List result = session.createQuery("from Compte").getResultList();
	                             session.getTransaction().commit();
	                     
	                     
	                     	
	                     	 
	                     	
	                     		List<Compte> listeUsager = result;
	                     		for(Compte str:listeUsager) {
	                     			
	                     			String name = f2.toString();
	                     			name = name.replace("\"", "");
	                     			if(str.getNom().equals(name)) {
	                     				System.out.println("ok: "+str.getid_compte());
	                     			}
	                     		}
	                     		
	                     	
	                        
	                         
								session.close();*/
	                     }
	                 }  
	        	 }
	        	 System.out.println(res);
				return res;
	            
	             
	        }
		    public String[] verifNom() throws Exception {
		    
	        	 Authentication authentication = SecurityContextHolder.getContext().getAuthentication(); 
	        	 if(authentication != null) {
	        		 Object details = authentication.getDetails();     
	                 if ( details instanceof OAuth2AuthenticationDetails ){
	                     OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;

	                     Map<String, Object> decodedDetails = (Map<String, Object>)oAuth2AuthenticationDetails.getDecodedDetails();

	                     System.out.println( "My custom claim value: " + oAuth2AuthenticationDetails.getTokenValue() );
	                     String jwtToken =oAuth2AuthenticationDetails.getTokenValue();                     
	                     System.out.println("------------ Decode JWT ------------");
	                     String[] split_string = jwtToken.split("\\.");
	                     String base64EncodedHeader = split_string[0];
	                     String base64EncodedBody = split_string[1];
	                     String base64EncodedSignature = split_string[2];

	                     System.out.println("~~~~~~~~~ JWT Header ~~~~~~~");
	                     Base64 base64Url = new Base64(true);
	                     String header = new String(base64Url.decode(base64EncodedHeader));
	                     System.out.println("JWT Header : " + header);


	                     System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
	                     String body = new String(base64Url.decode(base64EncodedBody));
	                     System.out.println("JWT Body : "+body);  
	                     
	                     JsonParser parser = new JsonParser();

	                   

	                     JsonElement jsonTree = parser.parse(body);

	                     if(jsonTree.isJsonObject()){
	                         JsonObject jsonObject = jsonTree.getAsJsonObject();

	                     
	             			Session session = HibernateUtil.getSessionFactory().openSession();
	             			session.beginTransaction();

	                         JsonElement f2 = jsonObject.get("user_name");
	                         System.out.println("name"+f2.toString());
	                         String name = f2.toString();
               				name = name.replace("\"", "");
	                      
	                    	   List<Compte> cp =   session.createQuery("from Compte").getResultList();
		                        
	                  	for (int i = 0; i < cp.size(); i++) {
							if(cp.get(i).getNom().equals(name)) {
								String[] res = {cp.get(i).getNom(),cp.get(i).getPrenom()};
								return res;
							}
							
						}
	                  			
	                       
	                        
	                        
                  		
	                       
	                         
	                     /*
	                     		
	                             List result = session.createQuery("from Compte").getResultList();
	                             session.getTransaction().commit();
	                     
	                     
	                     	
	                     	 
	                     	
	                     		List<Compte> listeUsager = result;
	                     		for(Compte str:listeUsager) {
	                     			
	                     			String name = f2.toString();
	                     			name = name.replace("\"", "");
	                     			if(str.getNom().equals(name)) {
	                     				System.out.println("ok: "+str.getid_compte());
	                     			}
	                     		}
	                     		
	                     	
	                        
	                         
								session.close();*/
	                     }
	                 }  
	        	 }
	        	
				return null;
	            
	             
	        }
		    
		
		  //GET ALL
			@GetMapping(value="/{id}/solde")
			public ResponseEntity<?> getSoldeCompte(@PathVariable("id") Long id) throws Exception{
				HibernateUtil hb = new HibernateUtil();
				hb.setUp();
				if(verifID(id)) {
					Session session = HibernateUtil.getSessionFactory().openSession();
					session.beginTransaction();
					
					//Optional<Compte> cp = session.byId(Compte.class).loadOptional(id);
					
					
					
			        
			        
					return (ResponseEntity<?>) Optional.ofNullable(session.byId(Compte.class).loadOptional(id))
							.filter(Optional::isPresent).map(i -> {
								try {
									session.getTransaction().commit();
									session.close();
									return new ResponseEntity<>(intervenantToResource(i.get(),i.get().getSolde(),true),HttpStatus.OK);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									session.getTransaction().commit();
									session.close();
									return new ResponseEntity<>(HttpStatus.NOT_FOUND);
								}
								
							})
							.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			}
			
			
		
			
			@GetMapping(value="/{id}/cartes")
			public ResponseEntity<?> getCartes(@PathVariable("id") Long id) throws Exception{
				HibernateUtil hb = new HibernateUtil();
				hb.setUp();
				if(verifID(id)) {
					List<CarteBancaireBean> response = proxyCarte.getCartes((Math.toIntExact(id)));
					return new ResponseEntity <> (carteToResource(response, id), HttpStatus.OK);
					/*Session session = HibernateUtil.getSessionFactory().openSession();
					session.beginTransaction();
					
					//Optional<Compte> cp = session.byId(Compte.class).loadOptional(id);
					
					
					
			        
			        
					return (ResponseEntity<?>) Optional.ofNullable(session.byId(Compte.class).loadOptional(id))
							.filter(Optional::isPresent).map(i -> {
								try {
									CarteBancaireBean cb =  (CarteBancaireBean) session.get(CarteBancaireBean.class, i.get().getId_carte());
									if(cb == null) {
										return new ResponseEntity<>(HttpStatus.NOT_FOUND);
									}
									session.getTransaction().commit();
									session.close();
									return new ResponseEntity<>(intervenantToResource(i.get(),cb,true),HttpStatus.OK);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									session.getTransaction().commit();
									session.close();
									return new ResponseEntity<>(HttpStatus.NOT_FOUND);
								}
								
							})
							.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));*/
				}
				else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			}
			
		
			
		/*	// POST
			@PostMapping
			public ResponseEntity<?> newIntervenant(@RequestBody Intervenant intervenant) {
				intervenant.setId(UUID.randomUUID().toString());
				Intervenant saved = ir.save(intervenant);
				HttpHeaders responseHeader = new HttpHeaders();
				responseHeader.setLocation(linkTo(IntervenantRepresentation.class).slash(saved.getId()).toUri());
				return new ResponseEntity<> (null,responseHeader, HttpStatus.CREATED);
			}
			
			// DELETE
			@DeleteMapping(value="/{IntervenantId}")
			public ResponseEntity<?> deleteIntervenant(@PathVariable("IntervenantId") String id) {
				Optional<Intervenant> intervenant = ir.findById(id);
				if (intervenant.isPresent()) {
					ir.delete(intervenant.get());
				}
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			*/
			
			private Resources<Resource<Compte>> intervenantToResource(Iterable<Compte> intervenants) throws Exception{
				Link selfLink = linkTo(methodOn(ProductController.class).getAllIntervenants()).withSelfRel();
				List<Resource<Compte>> intervenantResources = new ArrayList();
				intervenants.forEach(intervenant->
					{
						try {
							intervenantResources.add(intervenantToResource(intervenant, false));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
				return new Resources<>(intervenantResources, selfLink);
				
			}
			
			private Resources<Resource<OperationBean>> operationToResource(Iterable<OperationBean> op, Long id) throws Exception{
				Link selfLink = linkTo(methodOn(ProductController.class).getAllIntervenants()).withSelfRel();
				List<Resource<OperationBean>> opereationRessources = new ArrayList();
				op.forEach(operation->
					{
						try {
							opereationRessources.add(operationToResource(operation,id, false));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
				return new Resources<>(opereationRessources, selfLink);
				
			}
			
			private Resources<Resource<TransfertBean>> transfertToResource(List<TransfertBean> response, Long id) throws Exception{
				Link selfLink = linkTo(methodOn(ProductController.class).getAllIntervenants()).withSelfRel();
				List<Resource<TransfertBean>> tfRessources = new ArrayList();
				response.forEach(operation->
					{
						try {
							tfRessources.add(transfertToResource(operation,id, false));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
				return new Resources<>(tfRessources, selfLink);
				
			}
			
			private Resources<Resource<CarteBancaireBean>> carteToResource(List<CarteBancaireBean> response, Long id) throws Exception{
				Link selfLink = linkTo(methodOn(ProductController.class).getAllIntervenants()).withSelfRel();
				List<Resource<CarteBancaireBean>> tfRessources = new ArrayList();
				response.forEach(operation->
					{
						try {
							tfRessources.add(carteToResource(operation,id, false));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
				return new Resources<>(tfRessources, selfLink);
				
			}
			

			private Resource<TransfertBean> transfertToResource(TransfertBean tfb, Long id, Boolean collection) throws Exception{
				Link selfLink = linkTo(ProductController.class)
						.slash(id)
						.slash("transferts")
						.slash(tfb.getIdtransfert())
						.withSelfRel();
				Link selfLink2 = linkTo(ProductController.class)
						.slash(id)
						.slash("transferts")
						.withRel("transferts");
				if (collection) {
					Link collectionLink = linkTo(methodOn(ProductController.class).getAllIntervenants())
							.withSelfRel();
					return new Resource<>(tfb, selfLink,selfLink2  , collectionLink);
				}else {
					return new Resource<>(tfb, selfLink,selfLink2);
				}
				
			}
			
			
			
			private Resource<OperationBean> operationToResource(OperationBean op, Long id, Boolean collection) throws Exception{
				Link selfLink = linkTo(ProductController.class)
						.slash(id)
						.slash("operations")
						.slash(op.getId())
						.withSelfRel();
				Link selfLink2 = linkTo(ProductController.class)
						.slash(id)
						.slash("operations")
						.withRel("operations");
				if (collection) {
					Link collectionLink = linkTo(methodOn(ProductController.class).getAllIntervenants())
							.slash(id)
							.withSelfRel();
					return new Resource<>(op, selfLink,selfLink2 , collectionLink);
				}else {
					return new Resource<>(op, selfLink,selfLink2);
				}
				
			}
			
			private Resource<Compte> intervenantToResource(Compte intervenant, Boolean collection) throws Exception{
				Link selfLink = linkTo(ProductController.class)
						.slash(intervenant.getid_compte())
						.withRel("compte");
				Link selfLink2 = linkTo(ProductController.class)
						.slash(intervenant.getid_compte())
						.slash("solde")
						.withRel("soldes");
				Link selfLink3 = linkTo(ProductController.class)
						.slash(intervenant.getid_compte())
						.slash("cartes")
						.withRel("cartes");
				Link selfLink4 = linkTo(ProductController.class)
						.slash(intervenant.getid_compte())
						.slash("operations")
						.withRel("operations");
				Link selfLink5 = linkTo(ProductController.class)
						.slash(intervenant.getid_compte())
						.slash("transferts")
						.withRel("transferts");
				if (collection) {
					Link collectionLink = linkTo(methodOn(ProductController.class).getAllIntervenants())
							.withSelfRel();
					return new Resource<>(intervenant, selfLink,selfLink2 ,selfLink3,selfLink4,selfLink5, collectionLink);
				}else {
					return new Resource<>(intervenant, selfLink,selfLink2,selfLink3,selfLink4,selfLink5);
				}
				
			}
			
			private Resource<Float> intervenantToResource(Compte intervenant, Float solde, Boolean collection) throws Exception{
				Link selfLink = linkTo(ProductController.class)
						.slash(intervenant.getid_compte())
						.withRel("compte");
				if (collection) {
					Link collectionLink = linkTo(methodOn(ProductController.class).getAllIntervenants())
							.withSelfRel();
					return new Resource<>(solde,selfLink, collectionLink);
				}else {
					return new Resource<>(solde,selfLink);
				}
				
			}
			
			private Resource<CarteBancaireBean> carteToResource(CarteBancaireBean cb, Long id, Boolean collection) throws Exception{
				Link selfLink = linkTo(ProductController.class)
						.slash(id)
						.slash("cartes")
						.slash(cb.getId_carte())
						.withSelfRel();
				Link selfLink2 = linkTo(ProductController.class)
						.slash(id)
						.slash("cartes")
						.withRel("cartes");
				if (collection) {
					Link collectionLink = linkTo(methodOn(ProductController.class).getAllIntervenants())
							.slash(id)
							.withSelfRel();
					return new Resource<>(cb, selfLink,selfLink2 ,collectionLink);
				}else {
					return new Resource<>(cb, selfLink, selfLink2);
				}
				
			}
			
			//PUT 
			@PutMapping(value="/{id}/cartes/{idop}")
			public ResponseEntity<?> putCarte(@PathVariable("id") Long id, @PathVariable("idop") Long idop, @RequestBody CarteBancaireBean cb) throws Exception {
				Optional<CarteBancaireBean> body = Optional.ofNullable(cb);
				HibernateUtil hb = new HibernateUtil();
				hb.setUp();
				Session session = HibernateUtil.getSessionFactory().openSession();
				session.beginTransaction();
				//cas ou je n'ai pas d'intervenant

		    	try {
					if(verifID(id)) {
						if(!body.isPresent()) {
							return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
						}
						
						Compte cp =  session.get(Compte.class, id);
						
						
						CarteBancaireBean ope = proxyCarte.changeCarte(Math.toIntExact(id), Math.toIntExact(idop), cb);
						
					    HttpHeaders responseHeader = new HttpHeaders();
			    		responseHeader.setLocation(linkTo(CarteBancaireBean.class).slash(ope.getId_carte()).toUri());
			    		return new ResponseEntity<> (null,responseHeader, HttpStatus.NO_CONTENT);
						
					}
					else {
						return new ResponseEntity<>(HttpStatus.FORBIDDEN);

					}
		    	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
			} 
			
			//PUT 
			@PutMapping(value="/{id}")
			public ResponseEntity<?> putCompte(@PathVariable("id") Long id, @RequestBody Compte cp) throws Exception {
				Optional<Compte> body = Optional.ofNullable(cp);
				HibernateUtil hb = new HibernateUtil();
				hb.setUp();
				Session session = HibernateUtil.getSessionFactory().openSession();
				session.beginTransaction();
				//cas ou je n'ai pas d'intervenant

		    	try {
					if(verifID(id)) {
						if(!body.isPresent()) {
							return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
						}
							Compte compte =  session.get(Compte.class, id);
				       		session.evict(compte);
				       		System.out.println(compte);
				       		compte.setAll(cp);
				       		System.out.println(cp);
				    		session.update(compte);
				    		
				    		session.getTransaction().commit();
				    	     session.close();
				    	    	
					    HttpHeaders responseHeader = new HttpHeaders();
			    		responseHeader.setLocation(linkTo(Compte.class).slash(cp.getid_compte()).toUri());
			    		return new ResponseEntity<> (null,responseHeader, HttpStatus.NO_CONTENT);
						
					}
					else {
						return new ResponseEntity<>(HttpStatus.FORBIDDEN);

					}
		    	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
			}
			
			
			 
		    

}
