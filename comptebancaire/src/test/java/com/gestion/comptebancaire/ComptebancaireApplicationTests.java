package com.gestion.comptebancaire;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.util.Arrays;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;

import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;


import com.gestion.comptebancaire.web.controller.HibernateUtil;
import com.gestion.comptebancaire.web.entity.Compte;
import com.jayway.jsonpath.JsonPath;


@RunWith(SpringRunner.class)

@SpringBootTest( webEnvironment = WebEnvironment.RANDOM_PORT)
public class ComptebancaireApplicationTests {


	
	@Autowired
	private TestRestTemplate restTemplate;


    
    @Test
	public void getOneApi() throws Exception {
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.getSessionFactory().openSession();
   		session.beginTransaction();
   		Date d = new Date();
		Compte i1 = new Compte((long) 100,"ok","ok",d,"france",100,"06","fze","zerez",1);
		session.save(i1);
		
		ResponseEntity<String> response = restTemplate.getForEntity("/comptes/"+i1.getid_compte(), String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
		//assertThat(response.getBody()).contains("ok");
		session.getTransaction().commit();
		session.close();
	}
	
    
	
	@Test
	public void notFOundApi() {
		ResponseEntity<String> response = restTemplate.getForEntity("/intervenant", String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}
	
	
	@Test
	public void putApi() throws Exception {
		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.getSessionFactory().openSession();
   		session.beginTransaction();
   		Date d = new Date();
		Compte i1 = new Compte((long) 100,"ok","ok",d,"france",100,"06","fze","zerez",1);
		session.save(i1);
		i1.setNom("alexis");
		
		HttpHeaders headers =new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<>(this.toJsonString(i1), headers);
		
		//tuilisation de lapi pour faire la modif
		restTemplate.put("/comptes/"+i1.getid_compte(), entity);
		ResponseEntity<String> response = restTemplate.getForEntity("/comptes/"+i1.getid_compte(), String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).contains("alexis");
		String code = JsonPath.read(response.getBody(), "$.nom");
		assertThat(code).isEqualTo(i1.getNom());
	}
	
	private String toJsonString(Object o) throws Exception{
		ObjectMapper map = new ObjectMapper();
		return map.writeValueAsString(o);	
	}
	
	
	
	
}
