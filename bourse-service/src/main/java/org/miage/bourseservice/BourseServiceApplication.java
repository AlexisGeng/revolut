package org.miage.bourseservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class BourseServiceApplication {
	
	//localhost:8000/change-devise/source/USD/cible/GBP
	public static void main(String[] args) {
		SpringApplication.run(BourseServiceApplication.class, args);
	}
}
