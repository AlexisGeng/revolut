package org.miage.bourseservice.boundary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.miage.bourseservice.entity.ValeurChange;

public interface ValeurChangeRepository extends JpaRepository<ValeurChange, Long> {

    ValeurChange findBySourceAndCible(String source, String cible);
    
}
