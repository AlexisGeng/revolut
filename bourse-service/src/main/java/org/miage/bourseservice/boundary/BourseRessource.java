package org.miage.bourseservice.boundary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.miage.bourseservice.entity.ValeurChange;

@RestController
public class BourseRessource {

    @Autowired
    private Environment environment;

    @Autowired
    private ValeurChangeRepository vcr;

    @GetMapping("/change-devise/source/{source}/cible/{cible}")
    public ValeurChange getValeurDeChange(@PathVariable String source, @PathVariable String cible) {
    	System.out.println(source);
    	System.out.println(cible);
        ValeurChange valeurChange = vcr.findBySourceAndCible(source, cible);
        valeurChange.setPort(1);

        return valeurChange;
    }
}
