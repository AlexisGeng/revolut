package org.miage.operationservice.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;



@Entity
public class Operation {
	
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date d;
	private String libelle;
	private BigDecimal montant;
	private BigDecimal tauxconversion;
	private String commercant;
	private String categorie;
	private String pays;
	private int idcompte;
	
	public Date getD() {
		return d;
	}
	public void setD(Date d) {
		this.d = d;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public BigDecimal getMontant() {
		return montant;
	}
	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}

	public String getCommercant() {
		return commercant;
	}
	public void setCommercant(String commercant) {
		this.commercant = commercant;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	
	public Operation() {
	
	}
	

	public Operation(Date d, String libelle, BigDecimal montant, BigDecimal tauxconversion, String commercant,
			String categorie, String pays, int idcompte) {
		super();
		this.d = d;
		this.libelle = libelle;
		this.montant = montant;
		this.tauxconversion = tauxconversion;
		this.commercant = commercant;
		this.categorie = categorie;
		this.pays = pays;
		this.idcompte = idcompte;
	}
	
	
	@Id
	@Column(name="id_operation")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getTauxconversion() {
		return tauxconversion;
	}
	public void setTauxconversion(BigDecimal tauxconversion) {
		this.tauxconversion = tauxconversion;
	}
	public int getIdcompte() {
		return idcompte;
	}
	public void setIdcompte(int idcompte) {
		this.idcompte = idcompte;
	}
	@Override
	public String toString() {
		return "Operation [id=" + id + ", d=" + d + ", libelle=" + libelle + ", montant=" + montant
				+ ", tauxconversion=" + tauxconversion + ", commercant=" + commercant + ", categorie=" + categorie
				+ ", pays=" + pays + ", idcompte=" + idcompte + "]";
	}
	
	
	
	
}
