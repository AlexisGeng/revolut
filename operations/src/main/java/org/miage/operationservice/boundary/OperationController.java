package org.miage.operationservice.boundary;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.hibernate.Session;

import org.miage.operationservice.entity.DeviseConversionBean;
import org.miage.operationservice.entity.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;










@RestController
public class OperationController {

    @Autowired
    private ChangeServiceProxy proxy;

    @GetMapping("/conversion-devise-feign/source/{source}/cible/{cible}/quantite/{qte}")
    public DeviseConversionBean conversionFeign(@PathVariable String source,
            @PathVariable String cible,
            @PathVariable BigDecimal qte) {

        DeviseConversionBean response = proxy.getValeurChange(source, cible);

        return new DeviseConversionBean(response.getId(), source, cible, response.getTauxConversion(), qte,
                qte.multiply(response.getTauxConversion()), response.getPort());
    }
    
    
   @GetMapping(value="/comptes")
	public ResponseEntity<?> getAllIntervenants() throws Exception{
		HibernateUtil hb = new HibernateUtil();
		hb.setUp();
		Session session = HibernateUtil.sessionFactory.openSession();
		session.beginTransaction();
        List<Operation> result = session.createQuery("from Operation").getResultList();
        session.getTransaction().commit();
        session.close();
		System.out.println(result);
		return new ResponseEntity <> (intervenantToResource(result), HttpStatus.OK);
		
		
		
    }
    
    /*@GetMapping(value="/comptes/{id}/operations")
   	public List<Operation> getAllIntervenantsA(@PathVariable("id") int id) throws Exception{
   		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
           List<Operation> result = session.createQuery("from Operation Where idcompte="+id).getResultList();
           session.getTransaction().commit();
           session.close();
   		return result;
   		
   		
   		
       }*/
    
    @GetMapping(value="/comptes/{id}")
   	public Operation getIntervenant(@PathVariable("id") long id) throws Exception{
   		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
 
   		Operation cp =  session.get(Operation.class, id);
        session.getTransaction().commit();
        session.close();
        return cp;
   		
   		
   		
       }
    
    
    
    
    private Resources<Resource<Operation>> intervenantToResource(Iterable<Operation> intervenants) throws Exception{
		Link selfLink = linkTo(methodOn(OperationController.class).getAllIntervenants()).withSelfRel();
		List<Resource<Operation>> intervenantResources = new ArrayList<Resource<Operation>>();
		intervenants.forEach(intervenant->
			{
				try {
					intervenantResources.add(intervenantToResource(intervenant, false));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		return new Resources<>(intervenantResources, selfLink);
		
	}
    
    
    private Resource<Operation> intervenantToResource(Operation intervenant, Boolean collection) throws Exception{
		Link selfLink = linkTo(OperationController.class)
				.slash(intervenant.getId())
				.withSelfRel();
		if (collection) {
			Link collectionLink = linkTo(methodOn(OperationController.class).getAllIntervenants())
					.withSelfRel();
			return new Resource<>(intervenant, selfLink, collectionLink);
		}else {
			return new Resource<>(intervenant, selfLink);
		}
		
	}
	


	
    
    @PostMapping(value="/comptes/{id}/operations")
   	public Operation ajoutOperation(@PathVariable("id") long id, @RequestBody Operation op) throws Exception{
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
    	
		
    		
       		Session session = HibernateUtil.sessionFactory.openSession();
       		session.beginTransaction();
     
       		System.out.println(op.toString());
       		System.out.println(op.getIdcompte());
       		op.setIdcompte((int) id);
       		Long ope =  (Long)session.save(op);
       		Operation cp =  session.get(Operation.class, ope);
            session.getTransaction().commit();
            session.close();
            System.out.println(cp.toString());
        return cp;
    
	
   		
       }
    
    @GetMapping("/comptes/{id}/operations/{idop}")
    public Operation getOperation(@PathVariable("id") int id, @PathVariable("idop") long idop) throws Exception {
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
   		System.out.println(idop);
   		Operation cp =  session.get(Operation.class, idop);
        session.getTransaction().commit();
        session.close();
        System.out.println(cp.toString());
   		return cp;
   		
    }
    
    @GetMapping("/comptes/{id}/operations")
    public  List<Operation>  getOperation(@PathVariable("id") int id, @RequestParam Map<String, String> params) throws Exception {
    	List<Operation> resultC = new ArrayList<Operation>();
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
   		List<Operation> result = session.createQuery("from Operation Where idcompte="+id).getResultList();
   		System.out.println(params.isEmpty());
   		if(params.isEmpty()) {
   			session.getTransaction().commit();
   			session.close();
   			return result;
   		}
   		else {
   			for (Map.Entry<String, String> entry : params.entrySet()) {
   	   			for (int i = 0; i < result.size(); i++) {
   	   				if(entry.getKey().equals("commercant")) {
   	   					if(entry.getValue().equals(result.get(i).getCommercant())) {
   	   						resultC.add(result.get(i));
   	   						return  resultC;
   	   					}
   	   					
   	   				}
   	   				else {
	   					return result;

   	   				}
   	   			}
   	   		}
   			
   		}

   		return null;
      
    
  
   		
    }

    

}
