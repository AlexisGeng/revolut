package org.miage.operationservice.boundary;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.hibernate.Session;

import org.miage.operationservice.entity.DeviseConversionBean;
import org.miage.operationservice.entity.Operation;
import org.miage.operationservice.entity.Transfert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class TransfertController {

    @Autowired
    private ChangeServiceProxy proxy;

       
    
    
	public ResponseEntity<?> getAllcomptes() throws Exception{
		HibernateUtil hb = new HibernateUtil();
		hb.setUp();
		Session session = HibernateUtil.sessionFactory.openSession();
		session.beginTransaction();
        List<Transfert> result = session.createQuery("from Transfert").getResultList();
        session.getTransaction().commit();
        session.close();
		System.out.println(result);
		return new ResponseEntity <> (intervenantToResource(result), HttpStatus.OK);
		
		
		
    }
    
    @GetMapping(value="/comptes/{id}/transferts")
   	public List<Transfert> getAllTransfert(@PathVariable("id") int id) throws Exception{
   		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
           List<Transfert> result = session.createQuery("from Transfert Where idcompte="+id).getResultList();
           session.getTransaction().commit();
           session.close();
   		return result;
   		
   		
   		
       }
    
   /* @GetMapping(value="/comptes/{id}")
   	public Operation getIntervenant(@PathVariable("id") long id) throws Exception{
   		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
 
   		Operation cp =  session.get(Operation.class, id);
        session.getTransaction().commit();
        session.close();
        return cp;
   		
   		
   		
       }*/
    
    
    
    
    private Resources<Resource<Transfert>> intervenantToResource(Iterable<Transfert> tf) throws Exception{
		Link selfLink = linkTo(methodOn(TransfertController.class).getAllcomptes()).withSelfRel();
		List<Resource<Transfert>> transfertRessources = new ArrayList<Resource<Transfert>>();
		tf.forEach(tfs->
			{
				try {
					transfertRessources.add(intervenantToResource(tfs, false));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		return new Resources<>(transfertRessources, selfLink);
		
	}
    
    
    private Resource<Transfert> intervenantToResource(Transfert tf, Boolean collection) throws Exception{
		Link selfLink = linkTo(TransfertController.class)
				.slash(tf.getIdtransfert())
				.withSelfRel();
		if (collection) {
			Link collectionLink = linkTo(methodOn(TransfertController.class).getAllcomptes())
					.withSelfRel();
			return new Resource<>(tf, selfLink, collectionLink);
		}else {
			return new Resource<>(tf, selfLink);
		}
		
	}
	


	
    
    @PostMapping(value="/comptes/{id}/transferts")
   	public Transfert ajoutOperation(@PathVariable("id") long id, @RequestBody Transfert op) throws Exception{
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
    	
		
    		
       		Session session = HibernateUtil.sessionFactory.openSession();
       		session.beginTransaction();
     
       		System.out.println(op.toString());
       		System.out.println(op.getIdcompte());
       		Long ope =  (Long)session.save(op);
       		Transfert cp =  session.get(Transfert.class, ope);
            session.getTransaction().commit();
            session.close();
            System.out.println(cp.toString());
        return cp;
    
	
   		
       }
    
    @GetMapping("/comptes/{id}/transferts/{idop}")
    public Transfert getOperation(@PathVariable("id") int id, @PathVariable("idop") long idop) throws Exception {
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
   		System.out.println(idop);
   		Transfert cp =  session.get(Transfert.class, idop);
        session.getTransaction().commit();
        session.close();
        System.out.println(cp.toString());
   		return cp;
   		
    }

    

}
