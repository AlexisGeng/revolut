package org.miage.operationservice.boundary;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hibernate.Session;
import org.miage.operationservice.entity.DeviseConversionBean;
import org.miage.operationservice.entity.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;








@RestController
public class DeviseConversionRessource {

    @Autowired
    private ChangeServiceProxy proxy;

  /*  @GetMapping("/conversion-devise-feign/source/{source}/cible/{cible}/quantite/{qte}")
    public DeviseConversionBean conversionFeign(@PathVariable String source,
            @PathVariable String cible,
            @PathVariable BigDecimal qte) {

        DeviseConversionBean response = proxy.getValeurChange(source, cible);

        return new DeviseConversionBean(response.getId(), source, cible, response.getTauxConversion(), qte,
                qte.multiply(response.getTauxConversion()), response.getPort());
    }
    
    
    @GetMapping(value="/comptes")
	public ResponseEntity<?> getAllIntervenants() throws Exception{
		HibernateUtil hb = new HibernateUtil();
		hb.setUp();
		Session session = HibernateUtil.sessionFactory.openSession();
		session.beginTransaction();
        List<Operation> result = session.createQuery("from Operation").getResultList();
        session.getTransaction().commit();
        session.close();
		System.out.println(result);
		return new ResponseEntity <> (intervenantToResource(result), HttpStatus.OK);
		
		
		
    }
    
    @GetMapping(value="/{id}/comptesAll")
   	public List<Operation> getAllIntervenantsA(@PathVariable("id") int id) throws Exception{
   		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
           List<Operation> result = session.createQuery("from Operation Where id_compte="+id).getResultList();
           session.getTransaction().commit();
           session.close();
   		return result;
   		
   		
   		
       }
    
    @GetMapping(value="/comptes/{id}")
   	public Operation getIntervenant(@PathVariable("id") long id) throws Exception{
   		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
 
   		Operation cp =  session.get(Operation.class, id);
        session.getTransaction().commit();
        session.close();
        return cp;
   		
   		
   		
       }
    
    
    
    
    private Resources<Resource<Operation>> intervenantToResource(Iterable<Operation> intervenants) throws Exception{
		Link selfLink = linkTo(methodOn(DeviseConversionRessource.class).getAllIntervenants()).withSelfRel();
		List<Resource<Operation>> intervenantResources = new ArrayList<Resource<Operation>>();
		intervenants.forEach(intervenant->
			{
				try {
					intervenantResources.add(intervenantToResource(intervenant, false));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		return new Resources<>(intervenantResources, selfLink);
		
	}
    
    
    private Resource<Operation> intervenantToResource(Operation intervenant, Boolean collection) throws Exception{
		Link selfLink = linkTo(DeviseConversionRessource.class)
				.slash(intervenant.getId())
				.withSelfRel();
		if (collection) {
			Link collectionLink = linkTo(methodOn(DeviseConversionRessource.class).getAllIntervenants())
					.withSelfRel();
			return new Resource<>(intervenant, selfLink, collectionLink);
		}else {
			return new Resource<>(intervenant, selfLink);
		}
		
	}*/
	

}
