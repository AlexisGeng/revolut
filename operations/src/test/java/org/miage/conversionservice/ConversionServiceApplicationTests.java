package org.miage.conversionservice;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;


import org.hibernate.Session;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miage.operationservice.boundary.HibernateUtil;
import org.miage.operationservice.entity.Operation;
import org.miage.operationservice.entity.Transfert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConversionServiceApplicationTests {

	/*@Autowired 
	Compte cp;*/
	
	@Autowired
	private TestRestTemplate restTemplate;

    
    @Test
	public void getOneApi() throws Exception {
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
   		Date d = new Date();
		Operation i1 = new Operation(d,"56154984",new BigDecimal(100),new BigDecimal(65),"billal0","commerce","france",1);
		session.save(i1);
		
		ResponseEntity<String> response = restTemplate.getForEntity("/operations/"+i1.getId(), String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
		//assertThat(response.getBody()).contains("ok");
		session.getTransaction().commit();
		session.close();
	}
    
    @Test
   	public void getOneApiT() throws Exception {
       	HibernateUtil hb = new HibernateUtil();
      		hb.setUp();
      		Session session = HibernateUtil.sessionFactory.openSession();
      		session.beginTransaction();
      		Date d = new Date();
      		Transfert i1 = new Transfert(d,"15161265",new Long(100),new Long(1),new Long(1));
   		session.save(i1);
   		
   		ResponseEntity<String> response = restTemplate.getForEntity("/operations/"+i1.getIdtransfert(), String.class);
   		
   		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
   		//assertThat(response.getBody()).contains("ok");
   		session.getTransaction().commit();
   		session.close();
   	}
	
    
	
	@Test
	public void notFOundApi() {
		ResponseEntity<String> response = restTemplate.getForEntity("/intervenant", String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}
	
	
	@Test
	public void postApi() throws Exception {
		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
   		Date d = new Date();
   		Operation i1 = new Operation(d,"first",new BigDecimal(100),new BigDecimal(65),"billal","commerce","france",1);
		session.save(i1);
		
		
		HttpHeaders headers =new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<>(this.toJsonString(i1), headers);
		
		//tuilisation de lapi pour faire la modif

		ResponseEntity<String> response = restTemplate.getForEntity("/operations/"+i1.getId(), String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).contains("billal");
		String code = JsonPath.read(response.getBody(), "$.commercant");
		assertThat(code).isEqualTo(i1.getCommercant());
	}
	
	@Test
	public void postApiT() throws Exception {
		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
   		Date d = new Date();
   		Transfert i1 = new Transfert(d,"15161265",new Long(100),new Long(1),new Long(1));
		session.save(i1);
		
		
		HttpHeaders headers =new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<>(this.toJsonString(i1), headers);
		
		//tuilisation de lapi pour faire la modif

		ResponseEntity<String> response = restTemplate.getForEntity("/operations/"+i1.getIdtransfert(), String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).contains("15161265");
		String code = JsonPath.read(response.getBody(), "$.iban");
		assertThat(code).isEqualTo(i1.getIban());
	}
	
	private String toJsonString(Object o) throws Exception{
		ObjectMapper map = new ObjectMapper();
		return map.writeValueAsString(o);	
	}

}
