

import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.m2.oauthservice.OauthServiceApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.response.Response;


@RunWith(SpringRunner.class)
@SpringBootTest(classes= OauthServiceApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class OauthApplicationTests {

 
    private String tokenValue = null;

    @Autowired
    private JwtTokenStore tokenStore;	
    
    @Before
    public void obtainAccessToken() throws Exception {
    	  
    	 final Map<String, String> params = new HashMap<String, String>();
         params.put("grant_type", "password");
         params.put("client_id", "html5");
         params.put("username", "olivie");
         params.put("password", "sesame");
         
         Response response = RestAssured.given().auth().preemptive().basic("html5", "thesecret").and().with().params(params).when().post("http://localhost:9191/oauth/token");

         tokenValue = response.jsonPath().getString("access_token");
    }
    
    @Test
    public void givenInvalidRole_whenGetSecureRequest_thenForbidden() throws Exception {
        final OAuth2Authentication auth = tokenStore.readAuthentication(tokenValue);
        assertTrue(auth.isAuthenticated());
    }
	 
	
   
	
}
