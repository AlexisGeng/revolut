package org.m2.oauthservice;

import java.util.stream.Stream;

import org.hibernate.Session;
import org.m2.oauthservice.boundary.AccountRepository;
import org.m2.oauthservice.boundary.HibernateUtil;
import org.m2.oauthservice.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.stereotype.Component;



@SpringBootApplication
@EnableResourceServer
public class OauthServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OauthServiceApplication.class, args);
    }
}

@Component
class AccountCLR implements CommandLineRunner {

    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AccountCLR(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... strings) throws Exception {
     	HibernateUtil hb = new HibernateUtil();
    		hb.setUp();
    		Session session = HibernateUtil.sessionFactory.openSession();
    		session.beginTransaction();
    		   Stream.of("olivie," + passwordEncoder.encode("sesame"))
               .map(x -> x.split(","))
               .forEach(tpl -> session.save(new Account(tpl[0], tpl[1], true)));
    		   Stream.of("admin," + passwordEncoder.encode("geng"))
               .map(x -> x.split(","))
               .forEach(tpl -> session.save(new Account(tpl[0], tpl[1], true)));
    		session.getTransaction().commit();
    		session.close();
    }
}
