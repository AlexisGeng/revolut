package org.miage.conversionservice;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.util.Arrays;
import java.util.Date;


import org.hibernate.Session;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miage.carteservice.boundary.HibernateUtil;
import org.miage.carteservice.entity.CarteBancaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConversionServiceApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

    
    @Test
	public void getOneApi() throws Exception {
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.getSessionFactory().openSession();
   		session.beginTransaction();
   		Date d = new Date();
		CarteBancaire i1 = new CarteBancaire("1234567891234567","1234","123",false,false,10000,false,false,1);
		session.save(i1);
		
		ResponseEntity<String> response = restTemplate.getForEntity("/comptes/"+i1.getIdcompte(), String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
		//assertThat(response.getBody()).contains("ok");
		session.getTransaction().commit();
		session.close();
	}
	
    
	
	@Test
	public void notFOundApi() {
		ResponseEntity<String> response = restTemplate.getForEntity("/intervenant", String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}
	
	
	@Test
	public void putApi() throws Exception {
		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.getSessionFactory().openSession();
   		session.beginTransaction();
   		Date d = new Date();
   		CarteBancaire i1 = new CarteBancaire("1234567891234567","1234","123",false,false,10000,false,false,1);
		session.save(i1);
		i1.setPlafond(100);
		
		HttpHeaders headers =new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<>(this.toJsonString(i1), headers);
		
		//tuilisation de lapi pour faire la modif
		restTemplate.put("/comptes/"+i1.getId_carte(), entity);
		ResponseEntity<String> response = restTemplate.getForEntity("/comptes/"+i1.getId_carte(), String.class);
		
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).contains("123");
		String code = JsonPath.read(response.getBody(), "$.numerocarte");
		assertThat(code).isEqualTo(i1.getNumerocarte());
	}
	
	private String toJsonString(Object o) throws Exception{
		ObjectMapper map = new ObjectMapper();
		return map.writeValueAsString(o);	
	}

}
