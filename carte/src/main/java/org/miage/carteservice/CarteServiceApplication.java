package org.miage.carteservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("org.miage.carteservice")
@EnableDiscoveryClient
public class CarteServiceApplication {
	//localhost:8100/conversion-devise-feign/source/EUR/cible/USD/quantite/100
	public static void main(String[] args) {
		SpringApplication.run(CarteServiceApplication.class, args);
	}
}
