package org.miage.carteservice.boundary;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.hibernate.Session;
import org.miage.carteservice.entity.CarteBancaire;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CarteController {

    
    
    /*(value="/comptes")
	public ResponseEntity<?> getAllIntervenants() throws Exception{
		HibernateUtil hb = new HibernateUtil();
		hb.setUp();
		Session session = HibernateUtil.sessionFactory.openSession();
		session.beginTransaction();
        List<Operation> result = session.createQuery("from Operation").getResultList();
        session.getTransaction().commit();
        session.close();
		System.out.println(result);
		return new ResponseEntity <> (intervenantToResource(result), HttpStatus.OK);
		
		
		
    }*/

    @GetMapping(value="/comptes/{id}/cartes")
   	public List<CarteBancaire> getAllcartes(@PathVariable("id") int id) throws Exception{
   		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		System.out.println(hb);
   		System.out.println(HibernateUtil.getSessionFactory());
   		Session session = hb.getSessionFactory().openSession();
   		session.beginTransaction();
        List<CarteBancaire> result = session.createQuery("from CarteBancaire Where idcompte="+id).getResultList();
        session.getTransaction().commit();
        session.close();
   		return result;
   		
   		
   		
       }
    
    
    @PostMapping(value="/comptes/{id}/cartes")
   	public CarteBancaire ajoutOperation(@PathVariable("id") long id, @RequestBody CarteBancaire op) throws Exception{
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
    	
		
    		
       		Session session = HibernateUtil.sessionFactory.openSession();
       		session.beginTransaction();
       		
       		op.setIdcompte((int) id);
       		Long ope =  (Long)session.save(op);
       		CarteBancaire cp =  session.get(CarteBancaire.class, ope);
       		
            session.getTransaction().commit();
            session.close();
            System.out.println(cp.toString());
        return cp;
    
	
   		
       }
    
    @GetMapping("/comptes/{id}/cartes/{idop}")
    public CarteBancaire getOperation(@PathVariable("id") int id, @PathVariable("idop") long idop) throws Exception {
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
   		System.out.println(idop);
   		CarteBancaire cp =  session.get(CarteBancaire.class, idop);
        session.getTransaction().commit();
        session.close();
        System.out.println(cp.toString());
   		return cp;
   		
    }
    
    
    @PutMapping(value="/comptes/{id}/cartes/{idop}")
   	public CarteBancaire ajoutOperation(@PathVariable("id") long id,@PathVariable("idop") long idop,  @RequestBody CarteBancaire op) throws Exception{
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
    	
		
    		
       		Session session = HibernateUtil.sessionFactory.openSession();
       		session.beginTransaction();
     
       	
       	
       		CarteBancaire cp =  session.get(CarteBancaire.class, idop);
       		if (cp == null) {
   			 session.getTransaction().commit();
   		     session.close();
   			return null;
   			
   		}
       		session.evict(cp);
    		cp.setAll(op);
    	
    		session.update(cp);
    		
    		session.getTransaction().commit();
    	     session.close();
    	     return cp;
       
   		
       }
    
    @DeleteMapping(value="/comptes/{id}/cartes/{idop}")
   	public void deleteCarte(@PathVariable("id") long id,@PathVariable("idop") long idop) throws Exception{
    	HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
    	
		
    		
       		Session session = HibernateUtil.sessionFactory.openSession();
       		session.beginTransaction();
     
       		CarteBancaire cp =  session.get(CarteBancaire.class, idop);
       		session.delete(cp);
       	
    		
    		session.getTransaction().commit();
    	     session.close();
    	   
       
   		
       }
    
    /*
    @GetMapping(value="/comptes/{id}")
   	public Operation getIntervenant(@PathVariable("id") long id) throws Exception{
   		HibernateUtil hb = new HibernateUtil();
   		hb.setUp();
   		Session session = HibernateUtil.sessionFactory.openSession();
   		session.beginTransaction();
 
   		Operation cp =  session.get(Operation.class, id);
        session.getTransaction().commit();
        session.close();
        return cp;
   		
   		
   		
       }
    
    
    
    
    private Resources<Resource<Operation>> intervenantToResource(Iterable<Operation> intervenants) throws Exception{
		Link selfLink = linkTo(methodOn(CarteController.class).getAllIntervenants()).withSelfRel();
		List<Resource<Operation>> intervenantResources = new ArrayList<Resource<Operation>>();
		intervenants.forEach(intervenant->
			{
				try {
					intervenantResources.add(intervenantToResource(intervenant, false));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		return new Resources<>(intervenantResources, selfLink);
		
	}
    
    
    private Resource<Operation> intervenantToResource(Operation intervenant, Boolean collection) throws Exception{
		Link selfLink = linkTo(CarteController.class)
				.slash(intervenant.getId())
				.withSelfRel();
		if (collection) {
			Link collectionLink = linkTo(methodOn(CarteController.class).getAllIntervenants())
					.withSelfRel();
			return new Resource<>(intervenant, selfLink, collectionLink);
		}else {
			return new Resource<>(intervenant, selfLink);
		}
		
	}
	


	
  
    

*/
    

}
